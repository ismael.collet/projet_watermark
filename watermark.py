from paillier import gen_keys, gen_r, pailler_encrypt, pailler_decrypt

# data = a list of bytes (int)
# watermark = a list of bits (int)
def watermark_plain_data(data, watermark, maximum=255):
    watermarked_data=data.copy()
    for i in range(len(data)):
        w = watermark[i%len(watermark)]
        if (data[i]%2) != w :
            if data[i]==maximum:
                watermarked_data[i]-=1
            else :
                watermarked_data[i]+=1
    return watermarked_data

# data = a list of bytes (int)
# watermark = a list of bits (int)
# public_key = N
def cypher_with_watermark(data, watermark, public_key):
    ciphered_data=list()
    for i in range(len(data)) :
        w = watermark[i%len(watermark)]
        while "Not watermarked" :
            r=gen_r(public_key)
            c=pailler_encrypt(data[i], public_key, r)
            if c%2 == w:
                ciphered_data.append(c)
                break
    return ciphered_data

# private_key =(N, phi)
def decypher_data(cyphered_data, private_key):
    decyphered_data=list()
    for c in cyphered_data:
        decyphered_data.append(
                pailler_decrypt(c, private_key[0], private_key[1]))
    return decyphered_data

########
# TEST #
########
def test():
    from random import randint
    data=list()
    for _ in range(10):
        data.append(randint(0,255))
    watermark=[1,0,1,0,1,0,1,0,1,0,1,0]
    private_key=gen_keys(8)
    public_key=private_key[0]
    watermarked_data = watermark_plain_data(data, watermark, maximum=255)
    cyphered_data=cypher_with_watermark(watermarked_data, watermark, public_key)
    decyphered_data=decypher_data(cyphered_data,private_key)
    print(data)
    print(watermarked_data)
    print(cyphered_data)
    print(decyphered_data)

#test()