# Tuto pour installer gmpy2 sur linux : https://neatpicks.wordpress.com/2018/05/27/installing-gmpy2-in-ubuntu-16-04-for-python-3/

from gmpy2 import next_prime, mpz, gcd, powmod, invert
from random import randint

def pailler_encrypt(m, N, r):
    return powmod(N+1, m, N**2)*powmod(r, N, N**2) % N**2

def pailler_decrypt(c, N, phi):
    x = powmod(c, phi, N**2) - 1
    m = ((x // N) * invert(phi, N)) % N
    return m

def gen_keys(size=512):
    p = random_prime_nb(size)
    q = random_prime_nb(size)
    N = p*q
    phi = mpz(p-1)*mpz(q-1)
    return (N, phi)

def gen_r(n,size=512):
    while True:
        r=random_prime_nb(size)
        if gcd(r,n)==1 :
            return r

def random_prime_nb(size):
    while True :
        n=next_prime(randint(0, 2**size))
        if size_of_bin(n)==size:
            return n

def size_of_bin(x):
    x=int(x)
    return len(bin(abs(x))[2:])

########
# TEST #
########
def test():
    size=512
    N, phi = gen_keys(size)

    for i in range(1,11):
        r = gen_r(N,size)
        c = pailler_encrypt(i, N, r)
        m = pailler_decrypt(c, N, phi)
        print("%d -> %d -> %d" % (i, c, m))

#test()










