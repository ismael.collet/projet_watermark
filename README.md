# Rapport du projet watermark

*Ismaël Collet, Affiba Ruth De Naomi Boua*

## Principe de fonctionnement

Notre travail consistait en l’implémentation d’un système de protection de données. En effet, ce système repose sur la modulation de l’indice de quantification (QIM) et le cryptosystème de Pailler. Les données à protéger sont d’abord tatouées avant d’être chiffrées. Dans ce système les étapes de chiffrement et déchiffrement sont totalement indépendantes des phases d’insertion et d’extraction du message tatoué. Ainsi, il n’est pas nécessaire d’avoir la clé de chiffrement pour insérer un message dans la donnée chiffrée.

## Description de chaque script

* Le script paillier.py contient les fonctions permettant d’implémenter le cryptosystème de Paillier (chiffrement, déchiffrement, et génération des clés).

* Le script gen_paillier_keys.py permet de générer un fichier contenant des clés de chiffrement et de déchiffrement.

* Le script watermark.py contient les fonctions permettant de tatouer et de chiffrer avec tatouage des données (une liste d’entiers).

* L’exécution du script watermark_list_of_bytes.py a pour effet de tatouer et de chiffrer avec tatouage un fichier json contenant une liste d’octets.

* L’exécution du script get_watermark.py a pour effet de lire le watermark contenu dans un fichier json.

## Comment utiliser notre code

Notre code est en **python 3**.

### Créer une clé de chiffrement

On peut créer une clé de chiffrement avec cette commande :
```
python gen_paillier_keys.py [--filename FILENAME] [--size SIZE]
```
avec :

FILENAME (optionnel) le nom du fichier où elle va etre stockée.

SIZE (optionnel) la taille des entiers p et q (8 par défaut).


Cette commande génère un fichier texte de ce type :
```
{
    "public_key": 44377,
    "private_key": 43956
}
```
### Tatouer un chiffrer une liste d’octets
On effectue cette opération de cette manière :
1.Créer un fichier texte contenant la donnée à tatouer, c’est à dire une liste d’entiers entre 0 et 255 au format json (cf. le fichier example_data).
2.Créer un fichier contenant le watermark, c’est à dire une liste de 0/1 au format json (cf. le fichier example_watermark).
3.Créer, si ce n’est pas déjà fait, un fichier contenant la clé de chiffrement (cf. la partie ci-dessus).
4.Exécuter la commande :]
```
python watermark_list_of_bytes.py [fichier contenant la donnée à tatouer] [fichier contenant le watermark] [fichier contenant les clés de chiffrement]]
```
Après avoir fait ces étapes, deux fichiers vont apparaître : watermarked_data et cyphered_data. Ils contiennent ce que leur nom indique au format json.
### Lire le watermark
Pour lire le watermark d’un fichier contenant une liste d’entiers (au format json), il faut éxecuter la commande :
```
python get_watermark.py [nom du fichier]
```
Le watermark sera écrit dans le terminal.

