from paillier import gen_keys
import argparse, json

DEFAULT_OUTPUT_FILE = "paillier_keys"
DEFAULT_KEY_LEN=8

parser = argparse.ArgumentParser()
parser.add_argument("--filename", help="The file where the keys will be stored.")
parser.add_argument("--size", help="The size of p and q in bits (8 by default).",type=int)
args = parser.parse_args()

if args.filename :
    outputfile=args.filename
else:
    outputfile=DEFAULT_OUTPUT_FILE

if args.size:
    keylen=args.keylen
else:
    keylen=DEFAULT_KEY_LEN

keys=gen_keys(keylen)

output={
        "public_key":int(keys[0]),
        "private_key":int(keys[1])
}

with open(outputfile, 'w') as f:
    json.dump(output, f, indent=4)
