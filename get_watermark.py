import argparse, json

parser = argparse.ArgumentParser()
parser.add_argument("data_file", help= "The file containing the watermarked data.")
args = parser.parse_args()

def get_watermark(watermarked_data):
    watermark=[]
    for i in range (len(watermarked_data)):
        watermark.append(watermarked_data[i]%2)
    return watermark

with open(args.data_file) as f:
  data = json.load(f)
watermark = get_watermark(data)

print("")
print("Watermark :")
print(watermark)
print("")