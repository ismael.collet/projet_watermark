from watermark import watermark_plain_data, cypher_with_watermark
import argparse, json

parser = argparse.ArgumentParser()
parser.add_argument("data_file", help= "The file containing the data (a list of int from 0 to 255).")
parser.add_argument("watermark_file", help= "The file ontaining the watermark (a list of bits).")
parser.add_argument("keys", help= "The keys for Paillier cryptography.")
args = parser.parse_args()

with open(args.data_file) as f:
  data = json.load(f)

with open(args.watermark_file) as f:
  watermark = json.load(f)

with open(args.keys) as f:
  keys = json.load(f)

watermarked_data = watermark_plain_data(data, watermark)
cyphered_data = cypher_with_watermark(watermarked_data, watermark, keys["public_key"])

# convert cyphered_data to a list of int
for i in range(len(data)):
  cyphered_data[i]=int(cyphered_data[i])

with open("watermarked_data", 'w') as f:
  json.dump(watermarked_data, f)

with open("cyphered_data", 'w') as f:
  json.dump(cyphered_data, f)

print("")
print("The outputs are the files :")
print("\twatermarked_data")
print("\tcyphered_data")
print("")